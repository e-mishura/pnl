
import java.text.SimpleDateFormat
import java.util.Date

object Solution {

  val trades = Array(
      "2015-01-03,AAPL,B,50,80.0",
      "2015-01-05,AAPL,B,60,100.0",
      "2015-02-05,AAPL,S,70,130.0",
      "2015-02-08,AAPL,S,10,90.0",
      "2015-03-10,AAPL,S,80,120.0",
      "2015-03-12,AAPL,B,10,70.0",
      "2015-04-08,AAPL,B,70,160.0",
  )


  case class Position(quantity: Int, price: Double) {
    def notional: Double = quantity * price
  }


  case class Trade(date: Date, symbol: String, quantity: Int, price: Double) {
    def position: Position = Position(Math.abs(quantity), price)
  }


  sealed trait SymbolPosition{
    def symbol: String
    def profit: Double
    def positions: scala.collection.immutable.Queue[Position]

    def processTrade(trade: Trade): SymbolPosition
    def reverse(p: Position): SymbolPosition
    def calcProfitLoss(pnotional: Double, tnotional: Double): Double
    def updated(profit: Double = profit, positions: scala.collection.immutable.Queue[Position] = positions): SymbolPosition

    //(position)
    def fillQuantity(qty: Int, px: Double): SymbolPosition = {
      if(qty == 0)
        this
      else if(positions.isEmpty)
        this.reverse(Position(qty, px))
      else {
        val (nextPos, reminderPos) = positions.dequeue
        if (nextPos.quantity <= qty) {
          //next position partially fills qty
          val pnotional = nextPos.notional
          val tnotional = nextPos.quantity * px
          val prof = calcProfitLoss(pnotional, tnotional)
          val newPos = updated(profit + prof, reminderPos)
          newPos.fillQuantity(qty - nextPos.quantity, px)

        }
        else {
          //next position fills whole qty
          val pnotional = qty * nextPos.price
          val tnotional = qty * px
          val prof = calcProfitLoss(pnotional, tnotional)
          val reminder = nextPos.copy(quantity = nextPos.quantity - qty)
          updated(profit + prof, reminderPos.enqueue(reminder))
        }
      }

    }
  }
  case class SymbolLongPosition(symbol: String, profit: Double, positions: scala.collection.immutable.Queue[Position]) extends SymbolPosition {

    def reverse(p: Position): SymbolPosition = SymbolShortPosition(symbol, profit, scala.collection.immutable.Queue[Position](p))
    def updated(profit: Double = profit, positions: scala.collection.immutable.Queue[Position] = positions):SymbolPosition = this.copy(profit = profit, positions = positions)
    def calcProfitLoss(pnotional: Double, tnotional: Double): Double = tnotional - pnotional


    def processTrade(trade: Trade): SymbolPosition = {
      if(trade.quantity > 0)
        this.copy(positions = positions.enqueue(trade.position)) //accumulate
      else
        fillQuantity(-trade.quantity, trade.price)

    }
  }

  case class SymbolShortPosition(symbol: String, profit: Double, positions: scala.collection.immutable.Queue[Position]) extends SymbolPosition {

    def reverse(p: Position): SymbolPosition = SymbolLongPosition(symbol, profit, scala.collection.immutable.Queue[Position](p))
    def updated(profit: Double = profit, positions: scala.collection.immutable.Queue[Position] = positions): SymbolPosition = this.copy(profit = profit, positions = positions)
    def calcProfitLoss(pnotional: Double, tnotional: Double): Double = pnotional - tnotional

    def processTrade(trade: Trade): SymbolPosition = {
      if(trade.quantity < 0)
        this.copy(positions = positions.enqueue(trade.position))
      else
        fillQuantity(trade.quantity, trade.price)
    }
  }

  object SymbolPosition {
    def apply(t: Trade): SymbolPosition = {
      if(t.quantity < 0)
        SymbolShortPosition(t.symbol, 0.0, scala.collection.immutable.Queue(t.position))
      else
        SymbolLongPosition(t.symbol, 0.0, scala.collection.immutable.Queue(t.position))
    }
  }

  // Complete the calculateTax function below.
  def calculateTax(trades: Array[String]): String = {

    val trds = trades
      .map(parseTrade)
      .filter(t => t.date.getYear()+1900 == 2015)

    val seed: scala.collection.immutable.Map[String, SymbolPosition] = scala.collection.immutable.Map()
    val positions = trds.foldLeft(seed){(pos, trade) =>
      val symbolPos = pos
        .get(trade.symbol)
        .map(_.processTrade(trade))
        .getOrElse(SymbolPosition(trade))

      pos.updated(trade.symbol, symbolPos)
    }

    val tax = positions.values.map(_.profit).sum * 0.25
    val currencyFormatter = java.text.NumberFormat.getCurrencyInstance
    currencyFormatter.format(tax)
  }

  val dateFormat = new SimpleDateFormat("yyyy-MM-dd")
  def parseTrade(raw: String): Trade = {
    val parts = raw.split(',')
    val date = dateFormat.parse(parts(0))
    val q = parts(3).toInt
    val quantity = if(parts(2)=="B") q else -q
    val price = parts(4).toDouble
    Trade(date, parts(1), quantity, price)
  }

  def main(args: Array[String]) {

    val res = calculateTax(trades)

    println(res)

  }
}