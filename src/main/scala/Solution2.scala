import java.time.LocalDate
import scala.collection.immutable.Queue

object Solution2 extends App {

  val trades = Stream(
    "2015-01-03,AAPL,B,50,80.0",
    "2015-01-05,AAPL,B,60,100.0",
    "2015-02-05,AAPL,S,70,130.0",
    "2015-02-08,AAPL,S,10,90.0",
    "2015-03-10,AAPL,S,80,120.0",
    "2015-03-12,AAPL,B,10,70.0",
    "2015-04-08,AAPL,B,70,160.0",
  )

  sealed trait Side
  case object Buy extends Side
  case object Sell extends Side

  case class Trade(date: LocalDate, symbol: String, side: Side, quantity: Int, price: Double) {
    def notional(qty: Int): Double = qty * price * (if(side==Buy) -1 else 1)
    def notional: Double = notional(quantity)
  }

  def parseTrade(raw: String): Trade = {
    val parts = raw.split(',')
    val date = LocalDate.parse(parts(0))
    val quantity = parts(3).toInt
    val side = if(parts(2)=="B") Buy else Sell
    val price = parts(4).toDouble
    Trade(date, parts(1), side, quantity, price)
  }

  case class Position(profitLoss: Double, openPositions: Queue[Trade]) {
    def side: Side = openPositions.head.side
    def isSameSide(tx: Trade): Boolean = tx.side == side

    def processTrade(tx: Trade): Position = {
      if(tx.quantity == 0)
        this
      else if(openPositions.isEmpty)
        Position(profitLoss, Queue(tx))
      else if(isSameSide(tx))
        Position(profitLoss, openPositions.enqueue(tx))
      else {
        val (nextPos, nextOpen) = openPositions.dequeue
        if(nextPos.quantity <= tx.quantity) {
          //nextPos partially fulfill tx
          val pnotional = nextPos.notional
          val tnotional = tx.notional(nextPos.quantity)
          val pl = pnotional + tnotional
          val next = Position(profitLoss + pl, nextOpen)
          next.processTrade(tx.copy(quantity = tx.quantity - nextPos.quantity))
        }
        else { //pos.q > tx.q
          //nextPos fulfill whole tx quantity
          val pnotional = nextPos.notional(tx.quantity)
          val tnotional = tx.notional
          val pl = pnotional + tnotional
          val reminder = nextPos.copy(quantity = nextPos.quantity - tx.quantity)
          Position(profitLoss + pl, nextOpen.enqueue(reminder))
        }
      }
    }

  }

  object Position{
    def apply(tx: Trade): Position = Position(0.0, Queue(tx))
  }

  def calculateTax(trades: Stream[String], taxRate: Double, year: Int): Double = {
    val txs = trades
      .map(parseTrade)
      .filter(t => t.date.getYear() == year)

    val seed: Map[String, Position] = Map()
    val positions = txs.foldLeft(seed){(pos, trade) =>
      val symbolPos = pos
        .get(trade.symbol)
        .map(_.processTrade(trade))
        .getOrElse(Position(trade))

      pos.updated(trade.symbol, symbolPos)
    }
    val profitLoss = positions.values.map(_.profitLoss).sum
    if(profitLoss<0) 0.0 else profitLoss * taxRate
  }

  println(calculateTax(trades, 0.25, 2015))
}
